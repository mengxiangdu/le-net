# LeNet

#### 介绍

用libtorch1.13.1实现的LeNet

#### 软件架构

本程序用的是VS2017，需要支持C++14的编译器才行（因为libtorch1.13.1是用C++14写的）

#### 安装教程

1.  不用安装
2.  需要的libtorch库可以到pytorch官网下载，选择C++版即可。我下载的是C++ CPU版的库
3.  MNIST数据集需要自己下载

#### 使用说明

1.  这个工程用了VS的属性表文件，直接下载可能无法打开项目，需要手动删除.vcxproj文件中的多余的.props项然后再打开项目并导入你自己的libtorch库
项目设置需要设置C++14标准；符合模式设为“否”以避免出现“std不明确的符号”这样的错误

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
